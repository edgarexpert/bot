<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['currency_from', 'currency_to', 'exchange_rate', 'amount_from', 'amount', 'account_id'];

    protected $dates = ['deleted_at'];

    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
