<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['id', 'user_id', 'currency', 'amount'];

    protected $dates = ['deleted_at'];

    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
