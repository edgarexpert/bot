<?php

namespace App\Services;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Http;

class TransactionService
{
    private $url;
    private $loginService;
    private $accountService;

    public function __construct(UrlGenerator $url, AccountService $accountService, LoginService $loginService)
    {
        $this->url = $url->to('/') . '/api/transactions';
        $this->accountService = $accountService;
        $this->loginService = $loginService;
    }

    public function movement($amountFrom, $currencyFrom)
    {
        $uri = $this->url . '?token=' . session('token');
        $logged = $this->loginService->getLoggedUser(session('token'));
        if (isset($logged['errors'])) {
            return $logged;
        }
        $accountCurrency = $this->accountService->get($logged['id'])['currency'];
        $rate = CurrencyService::getExchange(1, $currencyFrom, $accountCurrency)['amount'];
        $post = [
            "account_id" => $logged['id'],
            "currency_from" => $currencyFrom,
            "currency_to" => $accountCurrency,
            "exchange_rate" => $rate,
            "amount_from" => $amountFrom,
            "amount" => $amountFrom * $rate
        ];
        $response = Http::post($uri, $post);
        $result = $response->json();

        $updateAccount = $this->accountService->update($result['account_id'], $result['amount']);

        if (isset($updateAccount['errors'])) {
            return $updateAccount;
        }

        return $result;
    }
}
