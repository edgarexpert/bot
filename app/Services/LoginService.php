<?php

namespace App\Services;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Http;

class LoginService
{
    private $url;

    public function __construct(UrlGenerator $url)
    {
        $this->url = $url->to('/') . '/api/auth';
    }

    public function login($email, $password)
    {
        $uri = $this->url . '/login';
        $post = [
            "email" => $email,
            "password" => $password
        ];
        $response = Http::post($uri, $post);
        if ($response->successful()) {
            session(['token' => $response['access_token']]);
        }
        return $response->json();
    }

    public function getLoggedUser($token)
    {
        $uri = $this->url . '/user-profile?token=' . $token;
        $response = Http::get($uri);
        return $response->json();
    }

    public function logout()
    {
        $uri = $this->url . '/logout?token=' . session('token');
        $response = Http::post($uri);
        return $response->json();
    }
}
