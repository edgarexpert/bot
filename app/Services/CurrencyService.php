<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class CurrencyService extends Model
{
    public static function getExchange($amount, $currencyFrom, $currency)
    {
        $uri = env("API_CURRENCY") . '&from=' .
            $currencyFrom . '&to=' . $currency . '&amount=' . $amount;
        $response = Http::get($uri);
        return $response->json();
    }
}
