<?php

namespace App\Services;

use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Http;

class AccountService
{
    private $url;
    protected $loginService;

    public function __construct(UrlGenerator $url, LoginService $loginService)
    {
        $this->url = $url->to('/') . '/api/accounts/';
        $this->loginService = $loginService;
    }

    public function create($name, $email, $password, $currency)
    {
        $uri = route('api.auth.register');
        $post = [
            "name" => $name,
            "email" => $email,
            "password" => $password,
            "password_confirmation" => $password
        ];
        $response = Http::post($uri, $post);
        if ($response->successful()) {
            $this->loginService->logout();
            $login = $this->loginService->login($email, $password);
            if (isset($login['access_token'])) {
                $uriAccount = $this->url . 'new';
                $postAccount = [
                    "id" => $login['user']['id'],
                    "user_id" => $login['user']['id'],
                    "currency" => $currency
                ];
                $responseAccount = Http::post($uriAccount, $postAccount);
                return $responseAccount->json();
            }
        }
        return $response->json();
    }

    public function update($id, $amount)
    {
        $uri = $this->url . $id . '?token=' . session('token');
        $update = [
            "amount" => $this->get($id)['amount'] + $amount
        ];
        $response = Http::put($uri, $update);
        return $response->json();
    }

    public function get($id)
    {
        $uri = $this->url . $id . '?token=' . session('token');
        $response = Http::get($uri);
        return $response->json();
    }

}
