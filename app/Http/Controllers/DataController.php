<?php

namespace App\Http\Controllers;

class DataController extends Controller
{
    public function closed()
    {
        $errors[] = "Please Login to access this!";
        return response()->json(compact('errors'), 401);
    }
}
