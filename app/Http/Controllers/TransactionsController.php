<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Validator;

class TransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function findAll()
    {
        $data = Transaction::all();
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'account_id' => "required",
            'currency_from' => 'required|size:3',
            'currency_to' => 'required|size:3',
            'exchange_rate' => 'required|numeric',
            'amount_from' => 'required|numeric',
            'amount' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $transaction = new Transaction();
        $transaction->fill($data);
        $transaction->save();

        return response()->json($transaction, 201);
    }
}
