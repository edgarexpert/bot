<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;
use Validator;

class AccountsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['create']]);
    }

    public function findAll()
    {
        $data = Account::all();
        return response()->json($data);
    }

    public function findOne($id)
    {
        $data = Account::with('user')->find($id);
        if (!$data) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }
        return response()->json($data);
    }

    public function create(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'currency' => 'required|size:3'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $account = new Account();
        $account->fill($data);
        $account->save();
        return response()->json($account, 201);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'amount' => 'required|numeric|gte:0'
        ], [
            'amount.gte' => 'Sorry, not enough balance in your account!'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validation Failed',
                'errors' => $validator->errors()->all()
            ], 422);
        }

        $account = Account::find($id);
        if (!$account) {
            return response()->json([
                'message' => 'Record not found',
            ], 404);
        }
        $account->fill($data);
        $account->save();
        return response()->json($account);
    }
}
