<?php

namespace App\Http\Controllers;

use App\Services\AccountService;
use App\Services\CurrencyService;
use App\Services\LoginService;
use App\Services\TransactionService;
use Illuminate\Routing\UrlGenerator;

class BotManController extends Controller
{
    protected $url;
    protected $loginService;
    protected $accountService;
    protected $transactionService;

    public function __construct(UrlGenerator $url, LoginService $loginService, AccountService $accountService, TransactionService $transactionService)
    {
        $this->url = $url;
        $this->loginService = $loginService;
        $this->accountService = $accountService;
        $this->transactionService = $transactionService;
    }

    public function handle()
    {
        $botman = app('botman');

        $botman->hears(
            'Exchange {amount} {currencyFrom} to {currency}',
            function ($bot, $amount, $currencyFrom, $currency) {
                $bot->types();
                $result = CurrencyService::getExchange($amount, $currencyFrom, $currency);
                $bot->reply(sprintf(
                    "%s %s is equivalent to %s %s",
                    number_format($amount, 2),
                    $currencyFrom,
                    number_format($result['amount'], 2),
                    $currency
                ));
            }
        );

        $botman->hears('Show balance', function ($bot) {
            $bot->types();
            $result = $this->loginService->getLoggedUser(session('token'));
            if (isset($result['errors'])) {
                $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
            } else {
                $result = $this->accountService->get($result['id']);
                if (isset($result['errors'])) {
                    $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
                } else {
                    $bot->reply(sprintf(
                        "Your current account balance is: %s %s",
                        number_format($result['amount'], 2),
                        $result['currency']
                    ));
                }
            }
        });

        $botman->hears('Login {email} {password}', function ($bot, $email, $password) {
            $bot->types();
            $result = $this->loginService->login($email, $password);
            if (isset($result['errors'])) {
                $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
            } else {
                $bot->reply("Hi " . $this->loginService->getLoggedUser(session('token'))['name']);
            }
        });

        $botman->hears('Logout', function ($bot) {
            $bot->types();
            $this->loginService->logout();
            $bot->reply("Bye!");
        });

        $botman->hears('Register {name} {email} {password} {currency}', function ($bot, $name, $email, $password, $currency) {
            $bot->types();
            $result = $this->accountService->create($name, $email, $password, $currency);
            if (isset($result['errors'])) {
                $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
            } else {
                $bot->reply('Account created');
            }
        });

        $botman->hears('Deposit {amount} {currencyFrom}', function ($bot, $amountFrom, $currencyFrom) {
            $bot->types();
            $result = $this->transactionService->movement($amountFrom, $currencyFrom);
            if (isset($result['errors'])) {
                $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
            } else {
                $bot->reply(
                    sprintf(
                        "Deposit of %s %s was equivalent to %s %s",
                        number_format($amountFrom, 2),
                        $currencyFrom,
                        number_format($result['amount'], 2),
                        $result['currency_to']
                    )
                );
            }
        });

        $botman->hears('Withdraw {amount}', function ($bot, $amount) {
            $bot->types();
            $logged = $this->loginService->getLoggedUser(session('token'));
            if (isset($logged['errors'])) {
                $bot->reply(sprintf("Oops! %s", implode("\n", $logged['errors'])));
            } else {
                $result = $this->transactionService->movement(-$amount, $this->accountService->get($logged['id'])['currency']);
                if (isset($result['errors'])) {
                    $bot->reply(sprintf("Oops! %s", implode("\n", $result['errors'])));
                } else {
                    $bot->reply(
                        sprintf(
                            "Withdrawal of %s %s",
                            number_format($amount, 2),
                            $result['currency_to']
                        )
                    );
                }
            }
        });

        $botman->fallback(function ($bot) {
            $bot->types();
            $bot->reply(nl2br(
                "Please type one of these commands:\n
            * Exchange {amount} {currencyFrom} to {currency}\r\n
            * Login {email} {password}\r\n
            * Show balance\r\n
            * Deposit {amount} {currencyFrom}\r\n
            * Withdraw {amount}\r\n
            * Register {name} {email} {password} {currency}\r\n
            * Logout"
            ));
        });

        $botman->listen();
    }
}
