<?php

use Illuminate\Database\Seeder;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Edgar Ronchi',
            'email' => 'edgarexpert@gmail.com',
            'password' => '$2y$10$3WNQnxyQpNDw5rj9GlB3yu90qvukozebc8GXMwCT1FVOODMrgBYJO', //admin123
        ]);
    }
}
