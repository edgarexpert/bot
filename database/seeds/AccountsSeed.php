<?php

use Illuminate\Database\Seeder;

class AccountsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Account::create([
            'id' => '1',
            'user_id' => '1',
            'currency' => 'USD'
        ]);
    }
}
