<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bot Silva - Your assistant to currency and account!</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/assets/css/chat.min.css">
    <script src='https://cdn.jsdelivr.net/npm/botman-web-widget@0/build/js/widget.js'></script>
</head>
<body>
<script>
    var botmanWidget = {
        title: 'BOT SILVA: Currency and Account bot!',
        introMessage: "Hi! I'm your assistant to currency and account! Please type something...",
        aboutText: 'by Brazil',
        desktopWidth: '50%'
    };
</script>
<div>
    Currency Exchange: Powered by <a href="https://www.amdoren.com">Amdoren</a>
</div>
</body>
</html>
