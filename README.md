Create:
```
<VirtualHost *:80>
    DocumentRoot "/var/www/html/bot/public"
    ServerName bot.local.com
    ServerAlias bot.local.com

	<Directory "/var/www/html/bot/public">
        AllowOverride All
	</Directory>
</VirtualHost>
```

Add to /etc/hosts:
```
127.0.0.1 bot.local.com
```
Run:
```
- Configure the .env file with your MySql access data (user, password, database, host)
- composer install 
- php artisan migrate
- php artisan db:seed

Note: In the .env file, API_CURRENCY is already configured ;)

- php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider" 
```

To run using Docker:
```
- change the DB_HOST at .env to DB_HOST=db
- add to /etc/hosts: 172.20.0.2	nginx 
(this must be the IP of the bot-nginx container in the docker at the connect network to it)

- Run the commands: 

docker-compose build app
docker-compose up -d

docker-compose exec app composer install
docker-compose exec app php artisan migrate
docker-compose exec app php artisan db:seed
docker-compose exec app php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"

chmod -R 777 storage/

- Finally, access http://nginx/

```
