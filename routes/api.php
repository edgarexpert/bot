<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register')->name('api.auth.register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user-profile', 'AuthController@userProfile');
    Route::get('closed', 'DataController@closed')->name('closed');
});

Route::match('get', '/accounts', 'AccountsController@findAll');
Route::match('get', '/accounts/{id}', 'AccountsController@findOne');
Route::match('put', '/accounts/{id}', 'AccountsController@update');
Route::match('post', '/accounts/new', 'AccountsController@create');

Route::match('get', '/transactions', 'TransactionsController@findAll');
Route::match('post', '/transactions', 'TransactionsController@create');
